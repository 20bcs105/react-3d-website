import { Html, useProgress } from '@react-three/drei'

function Loader() {
  const { progress } = useProgress()
  return <center>{progress} % loaded</center>
}

export default Loader;