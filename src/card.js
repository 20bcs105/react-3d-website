function Card(props){
    return(
        <section className="card-wrapper">
            <div className="card">
                <div className="image-wrapper">
                    <img src={props.image}/>
                </div>
                <span className="card-heading">{props.heading}</span>
                <span className="card-description">{props.description}</span>
                <button value={props.value} className="card-btn" onClick={()=>{
                props.changePopup(true)
                props.changeModel("BMW")
                }}>
                    View Model
                </button>
            </div>

        </section>
    )
}

export default Card;