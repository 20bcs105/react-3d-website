import { Canvas } from '@react-three/fiber';
import { PresentationControls, Stage, useGLTF,OrbitControls } from '@react-three/drei';
import Loader from '../loader';
import Suspense from 'react'

function Model(props){
    const modelProps = props.model 
    const {scene} = useGLTF(modelProps.scene)
    return(
            <Canvas dpr={[1,2]} shadows camera={{fov:45}} style={{width:"100%",height:"100%"}}>
                    <color attach="background" args={["#101010"]}/>
                    <PresentationControls speed={1.5} global zoom={.5} polar={[-0.1,Math.PI / 4]}>
                    <Stage environment={null}>
                    <primitive object={scene} {...modelProps}></primitive>
                    </Stage>
                    </PresentationControls>
            </Canvas>
        ) 
}

export default Model;